# README

#### Código criado para disciplina "Monográfica" do curso de Ciência da Computação da UENF.
Esta é a terceira versão com a heurística de criação de tabela de horários baseadas nas disciplinas da grade curricular do curso de Ciência da Computação da UENF. Esta versão foi otimizada para minimizar o número de consultas ao banco de dados, otimizações lógicas na heurística além da inserção da matriz de preferência.

* Ruby version '2.4.1'

* Rails version '5.2.0'

Comandos para popular o database:

```sh
$ rails db:seed utils:teacher_generate_faker
```
