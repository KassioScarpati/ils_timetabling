# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

//= require jquery-ui/jquery-ui.min.js
//= require chosen/chosen.jquery.js
//= require fullcalendar/moment.min.js
//= require fullcalendar/fullcalendar.min.js
//= require iCheck/icheck.min.js
//= require fullcalendar/lang/pt-br