class Timetablings::StepTwoController < ApplicationController
  def index
    @curso = Course.find(course_id_params)
    @disciplinas = Discipline.by_course_ids(course_id_params)
    @period = period_params
    @professores = Teacher.actives
  end

  private

  def period_params
    params[:period]
  end

  def course_id_params
    params[:course_id]
  end
end
