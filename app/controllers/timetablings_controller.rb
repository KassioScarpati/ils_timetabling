class TimetablingsController < ApplicationController
  def index
    @log_timetabling = LogTimetabling.all.order(id: :desc)
    @time_record = TimeRecord.last
  end

  def show
    @timetablings = Timetabling.collection_by_ids_ascending(timetabling_ids_params)
  end

  def create
    TimetablingService.create(params)

    redirect_to timetablings_index_path
  end

  private

  def timetabling_ids_params
    params[:ids_timetabling]
  end

end
