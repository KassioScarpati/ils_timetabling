class TimetablingsController::TimetablingService
  def self.create(params)

    course_id = Discipline.find(JSON[params["time_slot_1"]][0]).course_id

    n_periods = Course.find(course_id).n_periods

    iteracoes = params[:iteracoes]

    first_param = JSON[params["time_slot_1"]]

    #colecao de restricoes hard
    hr = []
    Restriction.hard(course_id).each do |r|
      hr << { week_day: r.week_day, start_time: r.start_time, end_time: r.end_time }
    end

    #colecao de restricoes soft
    sr = []
    Restriction.soft(course_id).each do |r|
      sr << { week_day: r.week_day, start_time: r.start_time, end_time: r.end_time, weight: r.weight }
    end

    # variavel para armazenar as hashes de timeslot
    timeslots = []

    #variavel para contar os timeslots equivalenta a utilizada na view step_2
    time_slot_count = 1

    # loop para armazenar os dados recebidos
    loop do
      if params.has_key?("time_slot_#{time_slot_count}")

        ts = JSON[params["time_slot_#{time_slot_count}"]]
        discipline = Discipline.find(ts[0])
        timeslots << { :discipline_id => ts[0],
                       :teacher_id => ts[1],
                       :period => discipline.period,
                       :credits => discipline.credits }

        time_slot_count += 1
      else
        break
      end
    end

    #registra tempo de inicio
    time_record = TimeRecord.create(start_time: Time.now)

    HeuristicMonography.execucao_iterada(iteracoes, timeslots, course_id, n_periods, hr, sr)

    #registra tempo de finalizacao
    time_record.update(end_time: Time.now)

  end
end
