module TimetablingsHelper

  def retorna_hash_timetabling(t_id)

    tm = []
    period = Timetabling.find(t_id).period
    timeslots = Timetabling.find(t_id).timeslots

    timeslots.each do |t|
      tm << { week_day: t.week_day,
              start_time: t.start_time,
              end_time: t.end_time,
              teacher_id: t.teacher_id,
              discipline_id: t.discipline_id,
              period: period,
              credits: Discipline.find(t.discipline_id).credits }
    end
    return tm
  end

  #helper utilizado na view
  def retorna_hr(t_id)
  #colecao de restricoes hard
  course_id = Timetabling.find(t_id).course_id
  hr = []
  Restriction.where(restriction_type: 0, course_id: course_id, situation: true).each do |r|
    hr << { week_day: r.week_day,
        start_time: r.start_time,
        end_time: r.end_time }
  end
    return hr
  end

  #helper utilizado na view
  def retorna_sr(t_id)
  #colecao de restricoes soft
  course_id = Timetabling.find(t_id).course_id
  sr = []
  Restriction.where(restriction_type: 1, course_id: course_id, situation: true).each do |r|
    sr << { week_day: r.week_day,
        start_time: r.start_time,
        end_time: r.end_time,
        weight: r.weight }
  end
  return sr
  end

  #helper utilizado na view
  def retorna_disciplinas(period, course_id)
    Discipline.where(course_id: course_id, period: period).order(:name)
  end

  def calc_period(period)
    period == "Ímpar" ? 1 : 2
  end

  def checa_timetabling(timetabling, hr, sr)
    HeuristicMonography.checa_timetabling(timetabling, hr, sr)
  end
end
