class HeuristicMonography

  def self.execucao_iterada(n, timeslots, course_id, n_periods, hr, sr)
    #execucao_iterada(params[:iteracoes], timeslots, course_id)

    peso = 0
    iteracoes = n.to_i

    #salva tempo inicial
    #time_record = TimeRecord.create(start_time: Time.now)
    time_record_start = Time.now.to_s


    # cria tabelas iniciais
    timetablings = self.create_timetablings(timeslots, course_id, n_periods, hr, sr)

    # melhora resultado inicial e salva o peso final
    timetablings = self.melhora_timetabling(timetablings, n_periods)

    peso = self.retorna_peso_timetabligs(n_periods, timetablings, hr, sr)

    #salva e terna ids de timetablins
    ids = self.save_timetablings(timetablings, n_periods, course_id)

    #salva no log os ids que corresponde a um semestre
    LogTimetabling.create(log_timetabling: ids.to_json,
                          peso: peso,
                          n_iteration: 1)


    (iteracoes - 1).times do |iteration|

      peso_aux = 0

      tm_ativos = timetablings

      timetablings = self.create_timetablings(timeslots, course_id, n_periods, hr, sr)

      timetablings = self.melhora_timetabling(timetablings, n_periods)

      peso_aux = self.retorna_peso_timetabligs(n_periods, timetablings, hr, sr)

      # se o resultado encontrado em peso_aux for mais leve entao salva
      if peso > peso_aux
        puts '************ encontrou melhoria **************'
        # salva e registra log
        ids = self.save_timetablings(timetablings, n_periods, course_id)

        #salva no log os ids que corresponde a um semestre
        LogTimetabling.create(log_timetabling: ids.to_json,
                              peso: peso_aux,
                              n_iteration: iteration + 2)
        peso = peso_aux
      else
        # se nao for mais leve descarta a solucao encontrada
        timetablings = tm_ativos
      end
    end

    #salva tempo final
    #time_record.update(end_time: Time.now)
  end

  def self.checa_timetabling(timetabling, hr, sr)

    count = 0

    count += self.checa_soft_restrictions(timetabling, sr)
    count += self.checa_janelas(timetabling, sr, hr)
    count += self.checa_continuidade(timetabling)

    return count
  end

  private

  def self.save_timetablings(timetablings, n_periods, course_id)

    ids = []

    period_count = 1

    peso = 0
    while period_count <= n_periods
      timetabling = timetablings.select {|s| s[:period] == period_count }

      if timetabling.present?
        tabling = Timetabling.create(period: period_count, course_id: course_id)

        ids << tabling.id

        timetabling.each do |t|
          Timeslot.create(week_day: t[:week_day],
                          start_time: t[:start_time],
                          end_time: t[:end_time],
                          teacher_id: t[:teacher_id],
                          discipline_id: t[:discipline_id],
                          timetabling_id: tabling.id)
        end
      end
      period_count += 1
    end
    return ids
  end

  def self.retorna_peso_timetabligs(n_periods, timetablings, hr, sr)
    period_count = 1

    peso = 0
    while period_count <= n_periods
      timetabling = timetablings.select {|s| s[:period] == period_count }

      if timetabling.present?
        peso += self.checa_timetabling(timetabling, hr, sr)
      end
      period_count += 1
    end

    return peso
  end

  # peso 1 para cada hora de janela
  # metodo que checa o numero de janelas existentes desde o primeiro timeslot até o ultimo
  def self.checa_janelas(timetabling, soft_restrictions, hard_restrictions)

    #auxiliares para definir criterio de parada para ultima aula
    #quando chega a ultima aula daquela semana, para de contar as janelas
    #porque quanto mais compacta uma tabela de horários melhor

    weight_count = 0

    tabling_aux = timetabling

    tabling_aux += soft_restrictions

    tabling_aux += hard_restrictions

    tabling_aux.sort_by! { |a| [ a[:week_day].to_date, a[:start_time].to_time ] }

    #essa ordenacao garante q o ultimo elemento seja de fato o ultimo
    ts = timetabling.sort_by { |a| [ a[:week_day].to_date, a[:start_time].to_time ] }
    last_timeslot = ts.last


    aux_boolean = false


    (("Sunday".to_date)..("Saturday".to_date)).each do |w|

      day_tabling = tabling_aux.select {|t| t[:week_day] == w.strftime("%A") }

    aux = 0
    day_tabling.each do |d|
      # checa se eh a ultima aula, se for nao contabiliza a janela
      if d.has_key?(:discipline_id)
        if d == last_timeslot
          aux_boolean = true
          break
        end
      end
      if day_tabling[aux + 1].present?
        weight_count += self.timeslot_diff(day_tabling[aux + 1][:start_time], d[:end_time])
      else
        weight_count += self.timeslot_diff("23:59", d[:end_time])
      end
      aux += 1

    end
    if aux_boolean
      break
    end
    end

    return weight_count
  end


  # metodo que checa se as restrições leves estao sendo satisfeitas
  # peso para cada soft_restriction eh definido no db
  def self.checa_soft_restrictions(timetabling, soft_restrictions)
    count = 0

    #checa cada restricao se esta sendo satisfeita
    soft_restrictions.each do |s|

      (timetabling.select {|s| s[:week_day] == s[:week_day] }).each do |t|
        #checagem se timeslot e restricao leve entrao em conflito
        # basica soft_restriction.startime <= timeslot.start_time and soft_restriction.end_time > timeslot.start_time
        # OR
        # soft_restriction.startime < timeslot.end_time and soft_restriction.end_time >= timeslot.end_time
        if ((s[:start_time] <= t[:start_time]) && (s[:end_time] > t[:start_time])) || ((s[:start_time] < t[:end_time]) && (s[:end_time] >= t[:end_time]))
          count += s[:weight]
        end
      end
    end

    return count
  end

  # peso 4 para cada disciplina descontinua
  # metodo que checa a continuidade entre disciplinas que acontece mais de uma vez por semana
  def self.checa_continuidade(timetabling)
    weight_count = 0
    ts = timetabling
    #seleciona todos os timeslots que possuem a mesma disciplina

    dup_disciplines = ts.select{|element| (ts.select{|e2| e2[:discipline_id] == element[:discipline_id]}).count == 2 }

    ids_uniq_discipline = []

    dup_disciplines.each do |d|
      ids_uniq_discipline << d[:discipline_id]
    end

    ids_uniq_discipline = ids_uniq_discipline.uniq


    ids_uniq_discipline.each do |i|
      aux = ts.select{|e2| e2[:discipline_id] == i}

      unless aux.first[:start_time] == aux.second[:start_time]
        weight_count += 4
      end
    end

    return weight_count
  end

  #metodo que retorna a diferenca de horas entre dois timetablings
  def self.timeslot_diff(startime_timeslot_proximo, endtime_timeslot_atual)
    diff = startime_timeslot_proximo.to_time - endtime_timeslot_atual.to_time
    value = diff/3600.0

    return value.to_i
  end

  #tenta melhorar checando o criterio de continuidade
  def self.melhora_timetabling(timetablings, n_periods)
    timetablings_aux = timetablings.sort_by { |a| [ a[:week_day].to_date, a[:start_time].to_time ] }

    #obtem o timetabling
    period_count = 1

    # loop para iterar em cada periodo
    while period_count <= n_periods

    # gera uma colecao de timeslots com o periodo armazenado em period_count
    timetabling = timetablings.select {|s| s[:period] == period_count }

      timeslots = timetabling.sort_by { |a| [ a[:week_day].to_date, a[:start_time].to_time ] }

      aux_ids_repetidos = []

      # iteracao dos timeslots
      timeslots.each do |t|
        # pega timeslots com mesmo id de disciplina
        aux = timeslots.select {|s| s[:discipline_id] == t[:discipline_id]}
        # checa se existem de fato duas aulas de uma mesma disciplina naquele timetabling
        if aux.count == 2
          # ordena as duas disciplinas
          aux = aux.sort_by { |a| [ a[:week_day].to_date, a[:start_time].to_time ] }
          # checa continuidade
          if aux[0][:start_time] == aux[1][:start_time]
            #puts '************* OK *************'
          else
            # checa o tempo total da ultima disciplina igual pq pode ser diferente como em calculo 2
            h_second_timeslot = self.timeslot_sizing(aux[0][:credits]).second

            # calcula o tempo final com base no resultado de h_second_timeslot
            aux_hours_second_ts = (aux[0][:start_time].to_time + h_second_timeslot.hours).strftime("%H:%M")

            # cria um mapa de viabilidade onde se tiver um true dentro a mudanca eh inviavel
            aux_map = []
            # obtem do timetabling da turma os timeslots do dia daquela semana menos ele mesmo
            (timetablings.select {|s| s[:week_day] == aux[1][:week_day]} - [aux[1]]).each do |d|
              # checa se existe sobreposicao
              aux_map << self.checa_sobreposicao(d[:start_time], d[:end_time], aux[0][:start_time], aux_hours_second_ts)
            end

            # obtem do timetabling do professor os timeslots do dia daquela semana menos ele mesmo
            ((timetablings.select {|s| s[:teacher_id] == aux[1][:teacher_id]}).select {|s| s[:week_day] == aux[1][:week_day]} - [aux[1]]).each do |d|
              # checa se existe sobreposicao
              aux_map << self.checa_sobreposicao(d[:start_time], d[:end_time], aux[0][:start_time], aux_hours_second_ts)
            end
            # checa se a mudanca eh inviavel, se nao for ele realiza um update
            unless aux_map.include?(true)
              timetablings_aux -= [aux[1]]
              aux[1][:start_time] = aux[0][:start_time]
              aux[1][:end_time] = aux_hours_second_ts
              timetablings_aux += [aux[1]]
            else
              #print aux_map
              #puts
              #puts '******* nao foi possivel corrigir**********'
            end
          end
        end
      end

    period_count += 1
    end
    return timetablings_aux
  end

  def self.create_timetablings(timeslots, course_id, n_periods, hr, sr)
    period_count = 1

    timetabling = []


    # loop para iterar em cada periodo
    while period_count <= n_periods

      # gera uma colecao de timeslots com o periodo armazenado em period_count
      collection_timeslots = timeslots.select {|s| s[:period] == period_count }

      # checa se esta em branco, se nao estiver ele chama o metodo construtor do timetabling
      unless collection_timeslots.blank?
        # self.timetabling_constructor recebe uma colecao de timeslots do mesmo periodo e o curso
        # retorna uma tabela de horários pronta para ser salva
        timetabling = timetabling + self.timetabling_constructor(collection_timeslots, course_id, timetabling, hr, sr)
      end

      period_count += 1
    end

    return timetabling
  end

  def self.timetabling_constructor(collection_timeslots, course_id, timetabling, hr, sr)
    timetabling_aux = []

    #embaralha ordem de timeslots
    collection_timeslots.shuffle!

    collection_timeslots.each do |e|

      #colecao de restricoes rigidas ligadas ao professor e ao curso
      hard_restrictions = self.establish_hard_restrictions(e[:teacher_id], timetabling, hr)

      #colecao de restricoes leves ligadas ao curso
      soft_restrictions = self.establish_soft_restrictions(sr)

      #divide timeslots grandes, maiores que 3
      sizing_ts = self.timeslot_sizing(e[:credits])

      sizing_ts.each do |ts|
        #ts eh um valor inteiro que representa quantas horas aquela aula vai durar
        # chamada da funcao self.set_timeslot_parameters
        # timetabling << {week_day: "???",
        #                 start_time: "???",
        #                 end_time: "???",
        #                 teacher_id: e[:teacher_id],
        #                 discipline_id: e[:discipline_id],
        #                 period: e[:period],
        #                 credits: e[:credits],
        #                 timetabling_id: nil}
          timetabling_aux += self.set_timeslot_parameters(ts, e[:teacher_id], e[:discipline_id], e[:period], e[:credits], hard_restrictions, timetabling_aux, soft_restrictions)
      end
    end

    return timetabling_aux
  end

  # retorna uma hash contendo um timeslot pronto
  def self.set_timeslot_parameters(duracao, teacher_id, discipline_id, period, credits,hard_restrictions, timetabling, soft_restrictions)

    restrictions_collection = []

    # reuni apenas em uma variavel todos os timeslots existentes
    hard_restrictions.each do |h|
      restrictions_collection << h
    end

    timetabling.each do |t|
      restrictions_collection << t
    end

    soft_restrictions.each do |s|
      restrictions_collection << s
    end

    # ordena os timesslots por dia de semana e hora de inicio
    # para que os furos nos timeslots sejam identificados
    restrictions_collection.sort_by! { |a| [ a[:week_day].to_date, a[:start_time].to_time ] }

    # checa se tem um timeslot com a mesma disciplina (gemeo)
    if timetabling.any? { |t| t[:discipline_id] == discipline_id}
      # se existe um registro daquela disciplina entao ele tem que
      # pular um dia e tentar introduzir com a mesma hora inicial
      # se nao tiver naquela hora tem que ir varrendo nas horas a frente
      # se nao tiver pula mais um dia e continua varrendo nas horas a frente

      # ******* checando sempre as restricoes rigidas e se o timetabling possui aquele horario disponivel *****
      timeslot_gemeo = timetabling.detect { |t| t[:discipline_id] == discipline_id}

      #utilizando matriz de preferencia
      (self.matriz_preferencia(timeslot_gemeo[:week_day])).each do |w|
        #day_restrictions = restrictions_collection.where(week_day: w.strftime("%A"))
        day_restrictions = restrictions_collection.select {|r| r[:week_day] == w.strftime("%A") }

        aux = 0
        day_restrictions.each do |dr|
          if day_restrictions[aux + 1].present?
            if self.timeslots_diff_valid?(duracao, day_restrictions[aux + 1][:start_time], dr[:end_time])
              return [{week_day: w.strftime("%A"),
                      start_time: dr[:end_time],
                      end_time: (dr[:end_time].to_time + duracao.hours).strftime("%H:%M"),
                      teacher_id: teacher_id,
                      discipline_id: discipline_id,
                      period: period,
                      credits: credits,
                      timetabling_id: nil}]
            end
          else
            if self.timeslots_diff_valid?(duracao, "23:59", dr[:end_time])
              return [{week_day: w.strftime("%A"),
                      start_time: dr[:end_time],
                      end_time: (dr[:end_time].to_time + duracao.hours).strftime("%H:%M"),
                      teacher_id: teacher_id,
                      discipline_id: discipline_id,
                      period: period,
                      credits: credits,
                      timetabling_id: nil}]
            end
          end
          aux += 1
        end
      end
    # caso nao seja um timeslot com irmao gemeo
    else
      # ******* checando os horarios disponiviveis atraves das restricoes rigidas e do timetabling corrente *****
      (("Sunday".to_date)..("Saturday".to_date)).each do |w|
        #day_restrictions = restrictions_collection.where(week_day: w.strftime("%A"))
        day_restrictions = restrictions_collection.select {|r| r[:week_day] == w.strftime("%A") }

        aux = 0
        day_restrictions.each do |dr|
          if day_restrictions[aux + 1].present?
            if self.timeslots_diff_valid?(duracao, day_restrictions[aux + 1][:start_time], dr[:end_time])
              return [{week_day: w.strftime("%A"),
                      start_time: dr[:end_time],
                      end_time: (dr[:end_time].to_time + duracao.hours).strftime("%H:%M"),
                      teacher_id: teacher_id,
                      discipline_id: discipline_id,
                      period: period,
                      credits: credits,
                      timetabling_id: nil}]
            end
          else
            if self.timeslots_diff_valid?(duracao, "23:59", dr[:end_time])
              return [{week_day: w.strftime("%A"),
                      start_time: dr[:end_time],
                      end_time: (dr[:end_time].to_time + duracao.hours).strftime("%H:%M"),
                      teacher_id: teacher_id,
                      discipline_id: discipline_id,
                      period: period,
                      credits: credits,
                      timetabling_id: nil}]
            end
          end
          aux += 1
        end
      end
    end
    # esse metodo deve retornar um timeslot pronto para ser salvo
  end


  def self.establish_hard_restrictions(teacher_id, timetabling, hr)
    hard_restrictions = []


    # armazena em hard_restrictions referentes aos horarios de aula do professores
    (timetabling.select {|s| s[:teacher_id] == teacher_id }).each do |r|
      hard_restrictions << { week_day: r[:week_day], start_time: r[:start_time], end_time: r[:end_time] }
    end

    # armazena em hard_restrictions as restricoes do array de hashs hr recebido pela funcao
    hard_restrictions += hr

    return hard_restrictions
  end

  #reuni soft_restrictions
  def self.establish_soft_restrictions(sr)
    soft_restrictions = []

    soft_restrictions += sr

    return soft_restrictions
  end

  # retorna um array de horas por time slots
  # ex para 6 credidos retorna [3,3]
  # o criterio estabelecido divide por dois disciplinas com mais de 3 creditos
  # eh uma funcao simples mas que permite ir apenas num lugar para modificar esse criterio
  def self.timeslot_sizing(credits)
    retorno = []
    if credits > 3

      divisor = 2
      resto = credits % divisor
      quociente = credits / divisor

      divisor.times do
        retorno << quociente
      end
      retorno[0] = retorno[0] + resto
    else
      retorno << credits
    end
    retorno
  end

  # metodo que retorna se o timeslot cabe de fato no meio de dois timeslots de restricao
  def self.timeslots_diff_valid?(duracao, start_timeslot_proximo, end_timeslot_atual)
    diff = start_timeslot_proximo.to_time - end_timeslot_atual.to_time
    if (diff/3600.0) >= duracao
      return true
    else
      return false
    end
  end

  def self.matriz_preferencia(week_day)

    # não manjo bem de Inglês, não me julgue )=
    # Segunda-feira = Monday
    # terça         = Tuesday
    # Quarta-feira  = Wednesday
    # Quinta-feira  = Thursday
    # Sexta-feira   = Friday
    matriz = [{"Monday" => ["Wednesday".to_date, "Thursday".to_date, "Friday".to_date, "Tuesday".to_date, "Monday".to_date]},
    {"Tuesday" => ["Thursday".to_date, "Friday".to_date, "Wednesday".to_date, "Monday".to_date, "Tuesday".to_date]},
    {"Wednesday" => ["Friday".to_date, "Monday".to_date, "Tuesday".to_date, "Thursday".to_date, "Wednesday".to_date]},
    {"Thursday" => ["Tuesday".to_date, "Monday".to_date, "Wednesday".to_date, "Friday".to_date, "Thursday".to_date]},
    {"Friday" => ["Wednesday".to_date, "Tuesday".to_date, "Monday".to_date, "Thursday".to_date, "Friday".to_date]}]

    # find retorna a hash correspondente e [week_day] retorna o valor da hash que no caso é uma array

    return matriz.find {|x| x[week_day]}[week_day]
  end

  # ts e te representao um intervalo onde ts eh o start e te eh o end
  # ms e me representao um outro intervalo onde ms eh o start e o me eh o end
  def self.checa_sobreposicao(ts, te, ms, me)
    ts = Time.parse(ts)
    te = Time.parse(te)
    ms = Time.parse(ms)
    me = Time.parse(me)


    diff_t = te - ts
    diff_m = me - ms

    # esse if eh necessario porque se nao existice o metodo
    # so funcionaria para intervalos (ts te) >= (ms me)
    if diff_t >= diff_m

      aux1 = (ts <= ms) && (te > ms)
      aux2 = (ts < me) && (te >= me)

      if aux1 || aux2
        return true
      else
        return false
      end
    else
      aux1 = (ms <= ts) && (me > ts)
      aux2 = (ms < te) && (me >= te)

      if aux1 || aux2
        return true
      else
        return false
      end
    end
  end
end
