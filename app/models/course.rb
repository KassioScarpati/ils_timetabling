class Course < ApplicationRecord
	validates :name, uniqueness: true, presence: true
	has_many :restrictions
end
