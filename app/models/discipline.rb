class Discipline < ApplicationRecord
  belongs_to :course
  has_many :prerequisites

  validates :name, uniqueness: { scope: [:course_id, :name] }, presence: true


  scope :by_course_ids, -> (ids) { where(course_id: ids) }
end
