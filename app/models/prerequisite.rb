class Prerequisite < ApplicationRecord
  belongs_to :discipline
  validates :discipline_id, uniqueness: { scope: [:pre_req, :discipline_id] }
end
