class Restriction < ApplicationRecord
  belongs_to :course

  validates :week_day, :start_time, :end_time, :course_id, :restriction_type, presence: true
  validates :course_id, uniqueness: { scope: [:week_day, :start_time, :end_time, :situation] }

  enum restriction_type: [:hard, :soft]

  scope :hard, -> (course_id) { where(restriction_type: 0, course_id: course_id, situation: true) }
  scope :soft, -> (course_id) { where(restriction_type: 1, course_id: course_id, situation: true) }

end
