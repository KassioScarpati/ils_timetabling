class Teacher < ApplicationRecord
  has_many :timeslots

  scope :actives, -> { where(situation: true) }
end
