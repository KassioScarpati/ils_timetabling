class Timeslot < ApplicationRecord
  belongs_to :teacher
  belongs_to :discipline
  belongs_to :timetabling
end
