class Timetabling < ApplicationRecord
  belongs_to :course
  has_many :timeslots

  scope :collection_by_ids_ascending, -> (ids) { where(id: ids).order(period: :asc) }
  scope :order_by_period, -> { all.order(period: :asc).order(situation: :desc) }
end
