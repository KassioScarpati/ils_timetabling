Rails.application.routes.draw do

  namespace :timetablings do
    get 'step_one/index'
    post 'step_two/index'
  end
  #### timetablings_controller

  post 'timetablings/create'

  get 'timetablings/show'

  get 'timetablings/index'

  #### home_controller
  get 'home/index'

  root 'home#index'

end
