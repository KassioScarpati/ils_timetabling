class CreateRestrictions < ActiveRecord::Migration[5.2]
  def change
    create_table :restrictions do |t|
      t.string :week_day
      t.string :start_time
      t.string :end_time
      t.integer :restriction_type
      t.integer :weight
      t.references :course, foreign_key: true
      t.boolean :situation, :default => true

      t.timestamps
    end
  end
end
