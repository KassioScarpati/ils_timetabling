class CreateTimetablings < ActiveRecord::Migration[5.2]
  def change
    create_table :timetablings do |t|
      t.boolean :situation, :default => true
      t.integer :period
      t.references :course, foreign_key: true

      t.timestamps
    end
  end
end
