class CreateDisciplines < ActiveRecord::Migration[5.2]
  def change
    create_table :disciplines do |t|
      t.string :name
      t.integer :period
      t.integer :credits
      t.integer :n_hours
      t.boolean :situation, :default => true
      t.references :course, foreign_key: true

      t.timestamps
    end
  end
end
