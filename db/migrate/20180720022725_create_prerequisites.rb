class CreatePrerequisites < ActiveRecord::Migration[5.2]
  def change
    create_table :prerequisites do |t|
      t.references :discipline, foreign_key: true
      t.integer :pre_req
      t.boolean :situation, default: true

      t.timestamps
    end
  end
end
