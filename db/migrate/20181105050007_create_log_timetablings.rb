class CreateLogTimetablings < ActiveRecord::Migration[5.2]
  def change
    create_table :log_timetablings do |t|
      t.string :log_timetabling
      t.integer :peso
      t.integer :n_iteration

      t.timestamps
    end
  end
end
