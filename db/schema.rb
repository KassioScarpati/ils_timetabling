# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_20_233634) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.integer "n_periods"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "disciplines", force: :cascade do |t|
    t.string "name"
    t.integer "period"
    t.integer "credits"
    t.integer "n_hours"
    t.boolean "situation", default: true
    t.bigint "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_disciplines_on_course_id"
  end

  create_table "log_timetablings", force: :cascade do |t|
    t.string "log_timetabling"
    t.integer "peso"
    t.integer "n_iteration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "prerequisites", force: :cascade do |t|
    t.bigint "discipline_id"
    t.integer "pre_req"
    t.boolean "situation", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["discipline_id"], name: "index_prerequisites_on_discipline_id"
  end

  create_table "restrictions", force: :cascade do |t|
    t.string "week_day"
    t.string "start_time"
    t.string "end_time"
    t.integer "restriction_type"
    t.integer "weight"
    t.bigint "course_id"
    t.boolean "situation", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_restrictions_on_course_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.string "name"
    t.boolean "situation", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "time_records", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "timeslots", force: :cascade do |t|
    t.string "week_day"
    t.string "start_time"
    t.string "end_time"
    t.boolean "situation", default: true
    t.bigint "teacher_id"
    t.bigint "discipline_id"
    t.bigint "timetabling_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["discipline_id"], name: "index_timeslots_on_discipline_id"
    t.index ["teacher_id"], name: "index_timeslots_on_teacher_id"
    t.index ["timetabling_id"], name: "index_timeslots_on_timetabling_id"
  end

  create_table "timetablings", force: :cascade do |t|
    t.boolean "situation", default: true
    t.integer "period"
    t.bigint "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_timetablings_on_course_id"
  end

  add_foreign_key "disciplines", "courses"
  add_foreign_key "prerequisites", "disciplines"
  add_foreign_key "restrictions", "courses"
  add_foreign_key "timeslots", "disciplines"
  add_foreign_key "timeslots", "teachers"
  add_foreign_key "timeslots", "timetablings"
  add_foreign_key "timetablings", "courses"
end
