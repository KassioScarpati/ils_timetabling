# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

print 'Criando Curso Ciência da Computação... '

curso = Course.new(name: 'Ciência da Computação', n_periods: 10)

if curso.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

###############################################################################
puts

curso = Course.find_by(name: 'Ciência da Computação')

###############################################################################

print '>>> CADASTRANDO RESTRIÇÕES RÍGIDAS DO CURSO DE CIÊNCIA DA COMPUTAÇÃO '

# domingo
Restriction.create(week_day: "Sunday",   start_time: "00:00", end_time: "23:59", course_id: curso.id, restriction_type: 0)

# segunda
Restriction.create(week_day: "Monday",   start_time: "00:00", end_time: "08:00", course_id: curso.id, restriction_type: 0)
Restriction.create(week_day: "Monday",   start_time: "18:00", end_time: "23:59", course_id: curso.id, restriction_type: 0)

# terça
Restriction.create(week_day: "Tuesday",  start_time: "00:00", end_time: "08:00", course_id: curso.id, restriction_type: 0)
Restriction.create(week_day: "Tuesday",  start_time: "18:00", end_time: "23:59", course_id: curso.id, restriction_type: 0)

# quarta
Restriction.create(week_day: "Wednesday",start_time: "00:00", end_time: "08:00", course_id: curso.id, restriction_type: 0)
Restriction.create(week_day: "Wednesday",start_time: "18:00", end_time: "23:59", course_id: curso.id, restriction_type: 0)

# quinta
Restriction.create(week_day: "Thursday", start_time: "00:00", end_time: "08:00", course_id: curso.id, restriction_type: 0)
Restriction.create(week_day: "Thursday", start_time: "18:00", end_time: "23:59", course_id: curso.id, restriction_type: 0)

# sexta
Restriction.create(week_day: "Friday",   start_time: "00:00", end_time: "08:00", course_id: curso.id, restriction_type: 0)
Restriction.create(week_day: "Friday",   start_time: "18:00", end_time: "23:59", course_id: curso.id, restriction_type: 0)

# sábado
Restriction.create(week_day: "Saturday", start_time: "00:00", end_time: "23:59", course_id: curso.id, restriction_type: 0)

puts "[OK] <<<"

###############################################################################

print '>>> CADASTRANDO RESTRIÇÕES LEVES DO CURSO DE CIÊNCIA DA COMPUTAÇÃO '

# segunda
Restriction.create(week_day: "Monday",   start_time: "12:00", end_time: "14:00", course_id: curso.id, restriction_type: 1, weight: 5)
# terça
Restriction.create(week_day: "Tuesday",  start_time: "12:00", end_time: "14:00", course_id: curso.id, restriction_type: 1, weight: 5)
# quarta
Restriction.create(week_day: "Wednesday",  start_time: "12:00", end_time: "14:00", course_id: curso.id, restriction_type: 1, weight: 5)
# quinta
Restriction.create(week_day: "Thursday",   start_time: "12:00", end_time: "14:00", course_id: curso.id, restriction_type: 1, weight: 5)
# sexta
Restriction.create(week_day: "Friday",   start_time: "12:00", end_time: "14:00", course_id: curso.id, restriction_type: 1, weight: 5)

puts "[OK] <<<"

###############################################################################

puts '>>> CADASTRANDO DISCIPLINAS 1º PERÍODO <<< '

periodo = 1

######################

disc = Discipline.new(
  name: 'Geometria Analítica',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Cálculo Diferencial e Integral 1',
  course_id: curso.id,
  period: periodo,
  credits: 6,
  n_hours: 102
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Organização de Computadores',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Lógica Matemática',
  course_id: curso.id,
  period: periodo,
  credits: 3,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Programação 1',
  course_id: curso.id,
  period: periodo,
  credits: 3,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Introdução à Ciência da Computação',
  course_id: curso.id,
  period: periodo,
  credits: 2,
  n_hours: 34
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Inglês Instrumental 1',
  course_id: curso.id,
  period: periodo,
  credits: 2,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end


###############################################################################
puts

puts '>>> CADASTRANDO DISCIPLINAS 2º PERÍODO <<< '

periodo = 2

######################

disc = Discipline.new(
  name: 'Algebra Linear',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end
######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Geometria Analítica")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id,
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end
######################

disc = Discipline.new(
  name: 'Cálculo Diferencial e Integral 2',
  course_id: curso.id,
  period: periodo,
  credits: 5,
  n_hours: 85
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Cálculo Diferencial e Integral 1")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)
if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Física Geral 1',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Cálculo Diferencial e Integral 1")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Laboratório Física Geral 1',
  course_id: curso.id,
  period: periodo,
  credits: 1,
  n_hours: 34
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Lógica Digital',
  course_id: curso.id,
  period: periodo,
  credits: 3,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Organização de Computadores")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id,
)
if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Estruturas Discretas',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Lógica Matemática")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Programação 2',
  course_id: curso.id,
  period: periodo,
  credits: 3,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Programação 1")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id,
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Inglês Instrumental 2',
  course_id: curso.id,
  period: periodo,
  credits: 2,
  n_hours: 68
)
print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Inglês Instrumental 1")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id,
)
if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

###############################################################################
puts

puts '>>> CADASTRANDO DISCIPLINAS 3º PERÍODO <<< '

periodo = 3

######################

disc = Discipline.new(
  name: 'Métodos Matemáticos',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Cálculo Diferencial e Integral 2")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id,
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Cálculo Diferencial e Integral 3',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Cálculo Diferencial e Integral 2")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id,
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Física Geral 2',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Cálculo Diferencial e Integral 2")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  print '[OK] '
else
  print '[JÁ EXISTE] '
end

pre_req = Discipline.find_by(name: "Física Geral 1")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  print '[OK] '
else
  print '[JÁ EXISTE] '
end

pre_req = Discipline.find_by(name: "Laboratório Física Geral 1")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK] '
else
  puts '[JÁ EXISTE] '
end

######################

disc = Discipline.new(
  name: 'Laboratório Física Geral 2',
  course_id: curso.id,
  period: periodo,
  credits: 1,
  n_hours: 34
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Arquitetura de Computadores',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Lógica Digital")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Paradigmas de Linguagens de Programação',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Programação 2")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Estrutura de Dados 1',
  course_id: curso.id,
  period: periodo,
  credits: 3,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Programação 2")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end


###############################################################################
puts

puts '>>> CADASTRANDO DISCIPLINAS 4º PERÍODO <<< '

periodo = 4

######################

disc = Discipline.new(
  name: 'Cálculo Numérico',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Algebra Linear")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  print '[OK]'
else
  print '[JÁ EXISTE]'
end

pre_req = Discipline.find_by(name: "Métodos Matemáticos")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Estatística e Probabilidade',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Cálculo Diferencial e Integral 3")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Sistema Operacional',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Arquitetura de Computadores")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  print '[OK]'
else
  print '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Estrutura de Dados 2',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Estrutura de Dados 1")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Programação Orientada a Objetos',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Estrutura de Dados 1")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Análise e Projeto de Sistemas',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Programação 2")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  print '[OK]'
else
  print '[JÁ EXISTE]'
end

pre_req = Discipline.find_by(name: "Introdução à Ciência da Computação")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

###############################################################################
puts

puts '>>> CADASTRANDO DISCIPLINAS 5º PERÍODO <<< '

periodo = 5

######################

disc = Discipline.new(
  name: 'Processos Estocásticos',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Estatística e Probabilidade")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)
if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Redes de Computadores',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Sistema Operacional")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)
if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Linguagens Formais e Teoria da Computação',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Estruturas Discretas")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)
if prereq.save
  print '[OK]'
else
  print '[JÁ EXISTE]'
end

pre_req = Discipline.find_by(name: "Paradigmas de Linguagens de Programação")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Banco de Dados 1',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Estrutura de Dados 2")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Engenharia de Software',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Análise e Projeto de Sistemas")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Computação e Sociedade',
  course_id: curso.id,
  period: periodo,
  credits: 2,
  n_hours: 34
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

###############################################################################
puts

puts '>>> CADASTRANDO DISCIPLINAS 6º PERÍODO <<< '

periodo = 6

######################

disc = Discipline.new(
  name: 'Pesquisa Operacional',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Cálculo Numérico")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Introdução à Computação Gráfica',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Cálculo Numérico")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  print '[OK]'
else
  print '[JÁ EXISTE]'
end

pre_req = Discipline.find_by(name: "Processos Estocásticos")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  print '[OK]'
else
  print '[JÁ EXISTE]'
end

pre_req = Discipline.find_by(name: "Física Geral 2")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Compiladores',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Linguagens Formais e Teoria da Computação")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Banco de Dados 2',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Banco de Dados 1")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Inteligência Artificial',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Estrutura de Dados 2")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Paradigma OO para Desenvolvimento de Software',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Programação Orientada a Objetos")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  print '[OK]'
else
  print '[JÁ EXISTE]'
end

pre_req = Discipline.find_by(name: "Engenharia de Software")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

###############################################################################
puts

puts '>>> CADASTRANDO DISCIPLINAS 7º PERÍODO <<< '

periodo = 7

######################

disc = Discipline.new(
  name: 'Sistemas Distribuídos',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Redes de Computadores")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Interface Homem-Máquina',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Inteligência Artificial")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Teste de Software',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Paradigma OO para Desenvolvimento de Software")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id,
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Metodologia de Trabalho Científico',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

###############################################################################
puts

puts '>>> CADASTRANDO DISCIPLINAS 8º PERÍODO <<< '

periodo = 8

######################

disc = Discipline.new(
  name: 'Empreendedorismo 1',
  course_id: curso.id,
  period: periodo,
  credits: 4,
  n_hours: 68
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Pesquisa Operacional")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

###############################################################################
puts

puts '>>> CADASTRANDO DISCIPLINAS 9º PERÍODO <<< '

periodo = 9

######################

disc = Discipline.new(
  name: 'Projeto de Monografia',
  course_id: curso.id,
  period: periodo,
  credits: 2,
  n_hours: 136
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Empreendedorismo 1")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  print '[OK]'
else
  print '[JÁ EXISTE]'
end

pre_req = Discipline.find_by(name: "Introdução à Computação Gráfica")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  print '[OK]'
else
  print '[JÁ EXISTE]'
end

pre_req = Discipline.find_by(name: "Interface Homem-Máquina")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)
if prereq.save
  print '[OK]'
else
  print '[JÁ EXISTE]'
end

pre_req = Discipline.find_by(name: "Teste de Software")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end


###############################################################################
puts

puts '>>> CADASTRANDO DISCIPLINAS 10º PERÍODO <<< '

periodo = 10

######################

disc = Discipline.new(
  name: 'Estágio Supervisionado',
  course_id: curso.id,
  period: periodo,
  credits: 3,
  n_hours: 204
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Projeto de Monografia")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id,
)

if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

disc = Discipline.new(
  name: 'Monografia',
  course_id: curso.id,
  period: periodo,
  credits: 2,
  n_hours: 136
)

print "#{disc.name}... "

if disc.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end

######################

print "Cadastrando pré-requisitos de #{disc.name}"

disciplina = Discipline.find_by(name: disc.name)

pre_req = Discipline.find_by(name: "Projeto de Monografia")

prereq = Prerequisite.new(
  discipline_id: disciplina.id,
  pre_req: pre_req.id
)
if prereq.save
  puts '[OK]'
else
  puts '[JÁ EXISTE]'
end
