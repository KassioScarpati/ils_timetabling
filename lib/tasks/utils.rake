namespace :utils do
  desc "TODO"
  task teacher_generate_faker: :environment do

    print "Gerando Professores Fake... "
    # 10.times do
    #   Teacher.create(name: Faker::Name.name)
    # end

    print "Cadastrando professores"

    Teacher.create(name: "Professor 1")
    Teacher.create(name: "Professor 2")
    Teacher.create(name: "Professor 3")
    Teacher.create(name: "Professor 4")
    Teacher.create(name: "Professor 5")
    Teacher.create(name: "Professor 6")
    Teacher.create(name: "Professor 7")
    Teacher.create(name: "Professor 8")
    Teacher.create(name: "Professor 9")
    Teacher.create(name: "Professor 10")


    puts "[OK]"
  end

  task remove_teachers: :environment do

    print "Removendo Professores Fake... "
    Teacher.delete_all
    puts "[OK]"
  end

end
